package net.heuyathic.practice.refactor.ch1;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import net.heuyathic.practice.refactor.ch1.Customer;
import static org.assertj.core.api.Assertions.*;

public class CustomerTest {
	
	private Customer jack;
	private Movie regular;
	private Movie newRelease;
	private Movie children;
	
	@Before
	public void init(){
		jack = new Customer("Jack");
		regular = new Movie("Regular", Movie.REGULAR);
		newRelease = new Movie("NewRelease", Movie.NEW_RELEASE);
		children = new Movie("Children", Movie.CHILDREN);
	}
	
	@Test
	public void should_owe_0_when_registration(){
		assertThat(jack.statement()).isEqualTo(getExpect("", "", "0.0", "0"));
		assertThat(jack.htmlStatement()).isEqualTo(getExpectHtml("", "", "0.0", "0"));
	}
	
	@Test
	public void should_owe_20_when_regular_1_day_rental(){
		regular.setPriceCode(Movie.REGULAR);
		Rental regularOneDay = new Rental(regular, 1);
		jack.addRental(regularOneDay);
		assertThat(jack.statement()).isEqualTo(getExpect("Regular","2.0","2.0","1"));
		assertThat(jack.htmlStatement()).isEqualTo(getExpectHtml("Regular","2.0","2.0","1"));
	}
	
	@Test
	public void should_owe_35_when_regular_3_day_rental(){
		Rental regularOneDay = new Rental(regular, 3);
		jack.addRental(regularOneDay);
		assertThat(jack.statement()).isEqualTo(getExpect("Regular","3.5","3.5","1"));
	}
	
	@Test
	public void should_owe_30_when_new_release_1_day_rental(){
		Rental regularOneDay = new Rental(newRelease, 1);
		jack.addRental(regularOneDay);
		assertThat(jack.statement()).isEqualTo(getExpect("NewRelease","3.0","3.0","1"));
	}
	
	@Test
	public void should_owe_60_when_new_release_2_day_rental(){
		Rental regularOneDay = new Rental(newRelease, 2);
		jack.addRental(regularOneDay);
		assertThat(jack.statement()).isEqualTo(getExpect("NewRelease","6.0","6.0","2"));
	}
	
	@Test
	public void should_owe_15_when_children_1_day_rental(){
		Rental regularOneDay = new Rental(children, 1);
		jack.addRental(regularOneDay);
		assertThat(jack.statement()).isEqualTo(getExpect("Children","1.5","1.5","1"));
	}
	
	@Test
	public void should_owe_30_when_children_4_day_rental(){
		Rental regularOneDay = new Rental(children, 4);
		jack.addRental(regularOneDay);
		assertThat(jack.statement()).isEqualTo(getExpect("Children","3.0","3.0","1"));
	}
	
	@After
	public void teardown(){
		
	}
	
	String getExpect(String movie, String amount, String total, String point){
		String moviePart = movie.isEmpty() ? "" : "\t" + movie + "\t" + amount + "\n";
		return "Rental Record for " + jack.getName() + "\n"
				+ moviePart + "Amount owed is " + total + "\n"
				+ "You earned " + point + " frequent renter points";
	}
	
	String getExpectHtml(String movie, String amount, String total, String point){
		String moviePart = movie.isEmpty() ? "" : movie + ": " + amount + "<BR>\n";
		return "<H1>Rentals for <EM>" + jack.getName() + "</EM></H1><P>\n"
				+ moviePart + "<P>You owe <EM>" + total + "</EM><P>\n"
				+ "On this rental you earned <EM>" + point + "</EM> frequent renter points<P>";
	}
	
}
